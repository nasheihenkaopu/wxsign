<?php
namespace Nshkp;

use PHPUnit\Framework\Constraint\Exception;

class Cache
{
    protected static $prefix = 'nasheihenkaopu_';

    public static function put($path,$key,$value,$time){
        if(!is_dir($path)){
            throw new Exception('$path不是一个正确的文件路径');
        }

        if(!is_string($key)){
            throw new Exception('$key只能是字符串');
        }

        $file = md5($path.static::$prefix.$key);

        if($value === null){
            return unlink($path.'/'.$file);
        }

        if(!is_string($value)){
            $value = json_encode($value);
        }

        $data = array();
        $data['timestamp'] = time();
        $data['time'] = $time;
        $data['value'] = $value;

        return file_put_contents($path.'/'.$file,json_encode($data));
    }

    public static function get($path,$key)
    {
        if(!is_dir($path)){
            throw new Exception('$path不是一个正确的文件路径');
        }

        if(!is_string($key)){
            throw new Exception('$key只能是字符串');
        }   

        $file = md5($path.static::$prefix.$key);
        if(!is_file($path.'/'.$file)){
            return null;
        }

        $content = file_get_contents($path.'/'.$file);

        if(empty($content)){
            throw new Exception('读取缓存内容失败');
        }

        $content = json_decode($content,true);

        if(time() - $content['timestamp'] < $content['time']){
            return $content['value'];
        }else{
            unlink($path.'/'.$file);
            return null;
        }
    }
}