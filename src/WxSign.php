<?php 
namespace Nshkp;

use Nshkp\Cache as NshkpCache;
use Nshkp\Curl as NshkpCurl;

class WxSign
{
    protected $app_id;

    protected $app_secret;

    protected $cache_path;

    public function __construct($app_id,$app_secret,$cache_path)
    {
        $this->app_id     = $app_id;
        $this->app_secret = $app_secret;
        $this->cache_path = $cache_path;
    }

    protected function getWxToken()
    {
        $wx_access_token = NshkpCache::get($this->cache_path,'wx_access_token');
        if($wx_access_token){
            return json_decode($wx_access_token,true)['access_token'];
        }else{
            $res = NshkpCurl::request("https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid={$this->app_id}&secret={$this->app_secret}");
            NshkpCache::put($this->cache_path,'wx_access_token',$res, 6500);
            return json_decode($res,true)['access_token'];
        }
    }

    protected function getWxJsapiTicket()
    {
        $wx_access_token = $this->getWxToken();
        $jsapi_ticket = NshkpCache::get($this->cache_path,'wx_jsapi_ticket');
        if($jsapi_ticket){
            return json_decode($jsapi_ticket,true)['ticket'];
        }else{
            $res = NshkpCurl::request("https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token={$wx_access_token}&type=jsapi");
            NshkpCache::put($this->cache_path,'wx_jsapi_ticket',$res, 6500);
            return json_decode($res,true)['ticket'];
        }
    }

    public function sign()
    {
        $data['timestamp']    = time();
        $data['noncestr']     = 'Wm3WZYTPz0wzccnW';
        $data['url']          = $this->getCurUrl();
        $data['jsapi_ticket'] = $this->getWxJsapiTicket();
        
        ksort($data);
        $sign['sign'] = sha1(urldecode(http_build_query($data)));
        $sign['str']  = $data['noncestr'];
        $sign['time'] = $data['timestamp'];
        $sign['uri'] = $data['url'];
        return $sign;
    }

    protected function getCurUrl() {
        $url = 'http://';
        if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') {
            $url = 'https://';
        }
        return $url . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    }
}
